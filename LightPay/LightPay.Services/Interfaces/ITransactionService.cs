﻿using LightPay.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LightPay.Services.Interfaces
{
    public interface ITransactionService
    {
        Task<Transaction> MakePayment(string senderAccountNumber, decimal amount,
            string recieverAccountNumber, string description,bool isSaved);
    }
}
