﻿using LightPay.Data.Context;
using LightPay.Models;
using LightPay.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace LightPay.Services.Services
{
    public class TransactionService : ITransactionService
    {

        private readonly LightPayContext context;

        public TransactionService(LightPayContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<Transaction> MakePayment(string senderAccountNumber, decimal amount,
            string recieverAccountNumber, string description, bool isSaved)
        {
            var payment = new Transaction();

            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var senderAccount = await this.context.Accounts
                        .SingleOrDefaultAsync(t => t.AccountNumber == senderAccountNumber);

                    var recieverAccount = await this.context.Accounts
                        .SingleOrDefaultAsync(t => t.AccountNumber == recieverAccountNumber);

                    if (senderAccount == null)
                    {
                        throw new ArgumentException("Account with this number does not exist");
                    }

                    if (recieverAccount == null)
                    {
                        throw new ArgumentException("Account with this number does not exist");
                    }

                    if (amount <= 0)
                    {
                        throw new ArgumentException("Transfer amount must be positive number");
                    }

                    if (senderAccount.Balance < amount)
                    {
                        throw new ArgumentException("Insufficient funds");
                    }

                    payment = new Transaction()
                    {
                        SenderAccount = senderAccount,
                        RecieverAccount = recieverAccount,
                        Ammount = amount,
                        Description = description,
                        CreatedOn = DateTime.Now,
                        //IsSaved = isSaved;
                    };

                    if (!isSaved)
                    {
                        senderAccount.Balance -= amount;
                        recieverAccount.Balance += amount;
                    }

                    transaction.Commit();                    
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }

                return payment;

            }
        }
    }
}
