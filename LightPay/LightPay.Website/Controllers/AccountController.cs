﻿using LightPay.Services.Interfaces;
using LightPay.Website.Models;
using LightPay.Website.Paging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using LightPay.Models;
using Microsoft.EntityFrameworkCore;

namespace LightPay.Website.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountService accountService;

        public AccountController(IAccountService accountService)
        {
            this.accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));            
        }


        [HttpGet]
        [ResponseCache(Location = ResponseCacheLocation.Client, Duration = 60)]
        public async Task<IActionResult> AllAccounts(string sortOrder, int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["A/cNumSortParm"] = String.IsNullOrEmpty(sortOrder) ? "a/c_number_desc" : "";
            ViewData["BalanceSortParm"] = sortOrder == "Balance" ? "balance_desc" : "Balance";


            var accounts = (await this.accountService
                .GetAccountsAsync())
                .Select(a=>new ListAccountViewModel(a))
                .AsQueryable();


            switch (sortOrder)
            {
                case "a/c_number_desc":
                    accounts = accounts.OrderByDescending(a=>a.AccountNumber);
                    break;
                case "Balance":
                    accounts = accounts.OrderBy(a => a.Balance);
                    break;
                case "balance_desc":
                    accounts = accounts.OrderByDescending(a => a.Balance);
                    break;
                default:
                    accounts = accounts.OrderBy(a => a.Balance);
                    break;
            }

            int pageSize = 10;
            return View(await PaginatedList<ListAccountViewModel>.CreateAsync(accounts.AsNoTracking(), pageNumber ?? 1, pageSize));
        }


        [HttpGet]
        //[Authorize]
        public IActionResult RenameAccount(Guid id, string accountNumber, string nickname,decimal balance,string clientName)
        {
            var account = new RenameAccountViewModel()
            {
                Id = id,
                AccountNumber = accountNumber,
                Nickname = nickname,
                Balance=balance,
                ClientName=clientName
            };

            return View(account);
        }


        [HttpPost]
        //[Authorize]
        [ValidateAntiForgeryToken]

        // POST: Rename Data with AJAX
        public async Task<IActionResult> RenameAccount(RenameAccountViewModel accountModel)
        {
            if (!this.ModelState.IsValid)
            {
                return View(accountModel);
            }
            try
            {
                var account = await this.accountService.RenameAccountAsync(accountModel.AccountNumber, accountModel.Nickname);



                return RedirectToAction("AllAccounts","Account");
            }
            catch (ArgumentException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return View(accountModel);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAccounts()
        {
            var accounts = await this.accountService.GetAccountsAsync();

            return Json(accounts.Select(ac => new
            {
                Name = ac.Nickname,
                Balance=ac.Balance
            }).ToList()
            );
        }

    }
}