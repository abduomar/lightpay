﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightPay.Services.Interfaces;
using LightPay.Website.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LightPay.Website.Controllers
{
    public class TransactionController : Controller
    {
        private readonly ITransactionService transactionService;

        public TransactionController(ITransactionService transactionService)
        {
            this.transactionService = transactionService ?? throw new ArgumentNullException(nameof(transactionService));
        }

        public IActionResult AllTransactions()
        {
            return View();
        }


        [HttpGet]
        [ResponseCache(Location = ResponseCacheLocation.Client, Duration = 60)]
        public IActionResult CreateTransaction()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateTransaction([FromBody]MakePaymentViewModel paymentModel)
        {
            if (!this.ModelState.IsValid)
            {
                return View(paymentModel);
            }
            try
            {
                var transaction = await this.transactionService.MakePayment(paymentModel.SenderAccountNumber,
                    paymentModel.Ammount, paymentModel.RecieverAccountNumber, paymentModel.Description, false);

                this.TempData["Success-Message"] = "Successful payment";

                return new JsonResult(paymentModel);
                //return RedirectToAction("Index", "Home");
            }
            catch (ArgumentException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return View(paymentModel);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveTransaction([FromBody]MakePaymentViewModel paymentModel)
        {
            if (!this.ModelState.IsValid)
            {
                return View(paymentModel);
            }
            try
            {
                var transaction = await this.transactionService.MakePayment(paymentModel.SenderAccountNumber,
                    paymentModel.Ammount, paymentModel.RecieverAccountNumber, paymentModel.Description, true);

                this.TempData["Success-Message"] = "Successful payment";

                return new JsonResult(paymentModel);
                //return RedirectToAction("Index", "Home");
            }
            catch (ArgumentException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return View(paymentModel);
            }
        }
    }
}