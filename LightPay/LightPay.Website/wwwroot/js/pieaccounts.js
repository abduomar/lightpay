﻿

function createChart() {


    $("#chart").kendoChart({
        title: {
            position: "bottom",
            text: "Account Chart"
        },
        legend: {
            visible: false
        },
        chartArea: {
            background: ""
        },
        seriesDefaults: {
            labels: {
                visible: true,
                background: "transparent",
                template: "#= category #: \n #= value#%"
            }
        },
        series: [{
            type: "pie",
            startAngle: 150,
            data: $.ajax({
                type: "GET",
                url: "/Account/GetAccounts",
                success: function (account) {
                    console.log(account)
                    var s;

                    for (var i = 0; i < account.length; i++) {
                      
                        s += {
                            category: account[i].Name,
                            value: account[i].Balance,
                        }
                    }
                },
            })




            //[{
            //    category: "Asia",
            //    value: 53.8,
            //},
            //    {
            //        category: "Europe",
            //        value: 16.1,
            //    }, {
            //        category: "Latin America",
            //        value: 11.3,
            //    }, {
            //        category: "Africa",
            //        value: 9.6,
            //    }, {
            //        category: "Middle East",
            //        value: 5.2,
            //    }, {
            //        category: "North America",
            //        value: 3.6,
            //    }]
        }],
        tooltip: {
            visible: true,
            format: "{0}%"
        }
    });
}

$(document).ready(createChart);
$(document).bind("kendo:skinChange", createChart);
